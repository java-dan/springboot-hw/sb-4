import requestService from './requestService';

export const login = (data) => requestService
    .post('/auth/login', data);
