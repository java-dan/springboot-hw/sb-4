import axios from 'axios';

class RequestService {
    #baseUrl = 'http://localhost:9000/';

    #timeout = 6000;

    constructor() {
        this.config = {
            baseURL: this.#baseUrl,
            timeout: this.#timeout,
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'POST, GET, OPTIONS',
//                'Access-Control-Allow-Headers': 'Content-Type/Authorization'
            },
        }

        this.axios = axios.create(this.config);
    }

    #getConfig(config) {
        return {
            ...this.config,
            ...config,
            headers: {
                ...this.config.headers,
                ...config?.headers
            }
        }
    }

    get(url, config) {
        return this.axios.get(url, this.#getConfig(config))
    }

    post(url, data, config) {
        return this.axios.post(url, data, this.#getConfig(config))
    }

    put(url, data, config) {
        return this.axios.put(url, data, this.#getConfig(config))
    }

    delete(url, config) {
        return this.axios.delete(url, this.#getConfig(config))
    }
}

export default RequestService;