import request from './requestService';

export const getCustomerList = (c) => request.get('/customers', c);
