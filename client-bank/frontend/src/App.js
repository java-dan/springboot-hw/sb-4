import * as React from 'react';
import {Route, Routes} from "react-router-dom";

import SignIn from './components/SignIn';
import SignUp from './components/SignUp';
// import Home from './components/Main';
import Dashboard from './components/Dashboard';

import './App.css';

function App() {
  return (
    <Routes>
      <Route path='/login' element={<SignIn />} />
      <Route path='/signup' element={<SignUp />} />
      <Route path='/' element={<Dashboard/>} />
    </Routes>
  )
}

export default App;
