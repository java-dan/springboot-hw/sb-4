import app.controller.AccountController;
import app.dto.account.AccountResp;
import app.dto.customer.CustomerDto;
import app.models.Currency;
import app.service.AccountService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest(classes = AccountController.class)
@AutoConfigureMockMvc
public class AccountControllerTest {
    @MockBean
    private AccountService service;
    @Autowired
    private MockMvc mockMvc;
//
//    @Test
//    public void testGetAllAccounts() throws Exception {
//        List<AccountResp> accounts = new ArrayList<AccountResp>();
//        accounts.add(new AccountResp(1L, LocalDateTime.of(2022, 8, 18, 13, 18), LocalDateTime.of(2023, 8, 19, 13, 18), "test1", Currency.EUR, 1000.00, new CustomerDto(1L, "Test", "test@mail.com", 20, "+380980000000")));
//        accounts.add(new AccountResp(2L, LocalDateTime.of(2023, 5, 18, 13, 18), LocalDateTime.of(2023, 8, 19, 13, 18), "test2", Currency.GBP, 500.0, new CustomerDto(1L, "Test", "test@mail.com", 20, "+380980000000")));
//
//        given(service.getAll()).willReturn(accounts);
//
//        mockMvc.perform(get("/accounts"))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$").isArray())
//                .andExpect(jsonPath("$[0].id").value(1L))
//                .andExpect(jsonPath("$[0].number").value("test1"))
//                .andExpect(jsonPath("$[1].id").value(2L))
//                .andExpect(jsonPath("$[1].number").value("test2"));
//    }
}
