--/password: 123456
INSERT INTO customers (created_date, last_modified_date, age, email, name, password, phone_number) VALUES ('2019-03-27T10:15:30', '2019-03-27T10:15:30', 27, 'elena@mail.com', 'Elena', '{pbkdf2}e0a3207d3c778af161e020559803bc6b6c07788ec1e06bf3c5f4c1a1886b497aff94f26e0c0f4886', '+380634445566');
INSERT INTO customers (created_date, last_modified_date, age, email, name, password, phone_number) VALUES ('2023-08-23 12:42:17.832198', '2023-08-23 12:42:17.832198', 32, 'martin@mail.com', 'Martin', '{pbkdf2}e0a3207d3c778af161e020559803bc6b6c07788ec1e06bf3c5f4c1a1886b497aff94f26e0c0f4886', '+380637777566');
INSERT INTO customers (created_date, last_modified_date, age, email, name, password, phone_number) VALUES ('2023-08-23 12:42:17.832198', '2023-08-23 12:42:17.832198', 56, 'jack@mail.com', 'Jack', '{pbkdf2}e0a3207d3c778af161e020559803bc6b6c07788ec1e06bf3c5f4c1a1886b497aff94f26e0c0f4886', '+380656787566');
INSERT INTO customers (created_date, last_modified_date, age, email, name, password, phone_number) VALUES ('2023-08-23 12:42:17.832198', '2023-08-23 12:42:17.832198', 54, 'amanda@mail.com', 'Amanda', '{pbkdf2}e0a3207d3c778af161e020559803bc6b6c07788ec1e06bf3c5f4c1a1886b497aff94f26e0c0f4886', '+380656577566');
INSERT INTO customers (created_date, last_modified_date, age, email, name, password, phone_number) VALUES ('2023-08-23 12:42:17.832198', '2023-08-23 12:42:17.832198', 19, 'felix@mail.com', 'Felix', '{pbkdf2}e0a3207d3c778af161e020559803bc6b6c07788ec1e06bf3c5f4c1a1886b497aff94f26e0c0f4886', '+380656667566');
INSERT INTO customers (created_date, last_modified_date, age, email, name, password, phone_number) VALUES ('2023-08-23 12:42:17.832198', '2023-08-23 12:42:17.832198', 42, 'max@mail.com', 'Max', '{pbkdf2}e0a3207d3c778af161e020559803bc6b6c07788ec1e06bf3c5f4c1a1886b497aff94f26e0c0f4886', '+380676587566');
INSERT INTO customers (created_date, last_modified_date, age, email, name, password, phone_number) VALUES ('2023-08-23 12:42:17.832198', '2023-08-23 12:42:17.832198', 33, 'lisa@mail.com', 'Lisa', '{pbkdf2}e0a3207d3c778af161e020559803bc6b6c07788ec1e06bf3c5f4c1a1886b497aff94f26e0c0f4886', '+380656786786');
INSERT INTO customers (created_date, last_modified_date, age, email, name, password, phone_number) VALUES ('2023-08-23 12:42:17.832198', '2023-08-23 12:42:17.832198', 50, 'bernard@mail.com', 'Bernard', '{pbkdf2}e0a3207d3c778af161e020559803bc6b6c07788ec1e06bf3c5f4c1a1886b497aff94f26e0c0f4886', '+380634587566');
INSERT INTO customers (created_date, last_modified_date, age, email, name, password, phone_number) VALUES ('2023-08-23 12:42:17.832198', '2023-08-23 12:42:17.832198', 24, 'oleh@mail.com', 'Oleh', '{pbkdf2}e0a3207d3c778af161e020559803bc6b6c07788ec1e06bf3c5f4c1a1886b497aff94f26e0c0f4886', '+380656444566');
INSERT INTO customers (created_date, last_modified_date, age, email, name, password, phone_number) VALUES ('2023-08-23 12:42:17.832198', '2023-08-23 12:42:17.832198', 27, 'jina@mail.com', 'Jina', '{pbkdf2}e0a3207d3c778af161e020559803bc6b6c07788ec1e06bf3c5f4c1a1886b497aff94f26e0c0f4886', '+380656333566');
INSERT INTO customers (created_date, last_modified_date, age, email, name, password, phone_number) VALUES ('2023-08-23 12:42:17.832198', '2023-08-23 12:42:17.832198', 60, 'rachel@mail.com', 'Rachel', '{pbkdf2}e0a3207d3c778af161e020559803bc6b6c07788ec1e06bf3c5f4c1a1886b497aff94f26e0c0f4886', '+380656444644');
--/password: qwerty
INSERT INTO customers (created_date, last_modified_date, age, email, name, password, phone_number) VALUES ('2023-08-23 12:42:17.832198', '2023-08-23 12:42:17.832198', 47, 'qwerty@mail.com', 'Jimmy', '{pbkdf2}f243eb2eee36a8080ddae58410b396bb30209b286ec2c6abb80e1e69b60b26515e6abe874d3bdea4', '+380611187566');

INSERT INTO accounts (created_date, last_modified_date, balance, currency, number, customer_id) VALUES ('2019-03-27T10:15:30', '2019-03-27T10:15:30', 4000.00, 'EUR', 'f2962718-8cf6-5040-b44d-f62c042f5d41', 1);
INSERT INTO accounts (created_date, last_modified_date, balance, currency, number, customer_id) VALUES ('2023-08-23 12:42:17.832198', '2023-08-23 12:42:17.832198', 38.70, 'USD', '96ad01f4-332b-59b9-bb72-131305c5636a', 1);
INSERT INTO accounts (created_date, last_modified_date, balance, currency, number, customer_id) VALUES ('2023-08-23 12:42:17.832198', '2023-08-23 12:42:17.832198', 500.00, 'UAH', 'f2962818-8cf6-5040-b42d-f62c042f5d41', 1);
INSERT INTO accounts (created_date, last_modified_date, balance, currency, number, customer_id) VALUES ('2023-08-23 12:42:17.832198', '2023-08-23 12:42:17.832198', 3000.00, 'EUR', '948cfe6a-26a6-43fd-844f-5f6bb4f3fbc3', 2);
INSERT INTO accounts (created_date, last_modified_date, balance, currency, number, customer_id) VALUES ('2023-08-23 12:42:17.832198', '2023-08-23 12:42:17.832198', 7500.00, 'EUR', '8e39f30d-6402-5d5d-85ac-86ee78b423cf', 2);
INSERT INTO accounts (created_date, last_modified_date, balance, currency, number, customer_id) VALUES ('2023-08-23 12:42:17.832198', '2023-08-23 12:42:17.832198', 0.00, 'UAH', 'c359657d-6995-5c3d-b85d-84871637954c', 2);
INSERT INTO accounts (created_date, last_modified_date, balance, currency, number, customer_id) VALUES ('2023-08-23 12:42:17.832198', '2023-08-23 12:42:17.832198', 2300.00, 'USD', '87ca6c12-ea4a-4d36-82f4-006bcec5d79c', 3);
--
INSERT INTO employers (created_date, last_modified_date, address, name) VALUES ('2019-03-27T10:15:30', '2019-03-27T10:15:30', 'USA', 'Apple');
INSERT INTO employers (created_date, last_modified_date, address, name) VALUES ('2023-08-23 12:42:17.832198', '2023-08-23 12:42:17.832198', 'Japan', 'Samsung');
INSERT INTO employers (created_date, last_modified_date, address, name) VALUES ('2023-08-23 12:42:17.832198', '2023-08-23 12:42:17.832198', 'London', 'Samsung');
INSERT INTO employers (created_date, last_modified_date, address, name) VALUES ('2023-08-23 12:42:17.832198', '2023-08-23 12:42:17.832198', 'Japan', 'Nokia');

INSERT INTO customers_employers (customers_id, employers_id) VALUES (1, 1);
INSERT INTO customers_employers (customers_id, employers_id) VALUES (2, 2);
INSERT INTO customers_employers (customers_id, employers_id) VALUES (3, 3);
INSERT INTO customers_employers (customers_id, employers_id) VALUES (2, 3);
INSERT INTO customers_employers (customers_id, employers_id) VALUES (1, 3);
INSERT INTO customers_employers (customers_id, employers_id) VALUES (1, 2);
INSERT INTO customers_employers (customers_id, employers_id) VALUES (3, 2);
