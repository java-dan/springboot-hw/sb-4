package app.mapper;

import app.dto.customer.CustomerReq;
import app.dto.customer.CustomerResp;
import app.models.Customer;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CustomerFacade {
    private final ModelMapper mapper;

    public Customer toCustomer(CustomerReq cr) {
        return mapper.map(cr, Customer.class);
    }

    public CustomerResp toCustomerResp(Customer c) {
        CustomerResp resp = mapper.map(c, CustomerResp.class);
        resp.setCreated_date(c.getCreatedDate());
        resp.setLast_modified_date(c.getLastModifiedDate());
        return resp;
    }
}
