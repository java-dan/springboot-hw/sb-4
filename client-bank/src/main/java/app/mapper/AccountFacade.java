package app.mapper;

import app.dto.account.AccountReq;
import app.dto.account.AccountResp;
import app.models.Account;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AccountFacade {
    private final ModelMapper mapper;

    public Account toAccount(AccountReq ar) {
        return mapper.map(ar, Account.class);
    }

    public AccountResp toAccountResp(Account a) {
        AccountResp resp = mapper.map(a, AccountResp.class);
        resp.setCreated_date(a.getCreatedDate());
        resp.setLast_modified_date(a.getLastModifiedDate());
        return resp;
    }
}
