package app.dao;

import app.models.Account;

public interface AccountDao extends DAO<Account> {
}
