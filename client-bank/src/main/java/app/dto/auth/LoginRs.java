package app.dto.auth;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class LoginRs {
    Boolean status;
    String error;
    String token;

    public static  LoginRs Ok(String token) {
        return new LoginRs(true, null, token);
    }
    public static   LoginRs Error(String message) {
        return new LoginRs(false, message, null);
    }
}
