package app.dto.customer;

import app.dto.account.AccountDto;
import app.dto.employer.EmployerDto;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class CustomerResp {
    private Long id;
    private LocalDateTime created_date;
    private LocalDateTime last_modified_date;
    private String name;
    private String email;
    private Integer age;
    private String password;
    private String phoneNumber;
    private List<AccountDto> accounts;
    private List<EmployerDto> employers;
}
