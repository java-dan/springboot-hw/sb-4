package app.dto.customer;

import javax.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CustomerReq {
    @Size(min = 2, message = "Name should be at least 2 characters")
    private String name;
    @Email(message = "Email is not valid", regexp = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$")
    private String email;
    @Min(value = 18, message = "User should be at least 18 years old")
    private Integer age;
    @Pattern(regexp = "^\\+[0-9]\\d{11}$", message = "Incorrect phone number")
    private String phoneNumber;
    @NotNull
    private String password;
}
