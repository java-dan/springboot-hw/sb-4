package app.dto.customer;

import lombok.Data;

@Data
public class CustomerDto {
    private Long id;
    private String name;
    private String email;
    private Integer age;
    private String phoneNumber;

    public CustomerDto() {}
    public CustomerDto(Long id, String name, String email, Integer age, String phoneNumber) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.age = age;
        this.phoneNumber = phoneNumber;
    }
}
