package app.dto.account;

import app.dto.customer.CustomerDto;
import app.models.Currency;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountResp {
    private Long id;
    private LocalDateTime created_date;
    private LocalDateTime last_modified_date;
    private String number;
    private Currency currency;
    private Double balance;
    private CustomerDto customer;
}
