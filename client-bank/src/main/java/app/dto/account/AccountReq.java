package app.dto.account;

import app.models.Currency;
import app.models.Customer;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.*;

@Data
@AllArgsConstructor
public class AccountReq {
    @NotNull(message = "Currency couldn't be null")
    private Currency currency;
    @PositiveOrZero
    private Customer customer;
}
