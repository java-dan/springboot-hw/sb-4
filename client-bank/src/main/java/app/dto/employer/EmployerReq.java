package app.dto.employer;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
public class EmployerReq {
    @Size(min = 3, message = "Name should be at least 3 characters")
    private String name;
    @Size(min = 3, message = "Address should be at least 3 characters")
    private String address;
}
