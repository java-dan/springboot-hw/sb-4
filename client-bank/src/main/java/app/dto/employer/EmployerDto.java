package app.dto.employer;

import lombok.Data;

@Data
public class EmployerDto {
    private Long id;
    private String name;
    private String address;
}
