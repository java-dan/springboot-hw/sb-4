package app.service;

import app.dto.employer.EmployerReq;
import app.dto.employer.EmployerResp;
import app.mapper.EmployerFacade;
import app.models.Customer;
import app.models.Employer;
import app.repo.CustomerRepo;
import app.repo.EmployerRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class EmployerService {
    private final CustomerRepo customerRepo;
    private final EmployerRepo employerRepo;
    private final EmployerFacade facade;

    public List<EmployerResp> getAll() {
        return employerRepo.findAll()
                .stream()
                .map(facade::toEmployerResp)
                .collect(Collectors.toList());
    }

    public Optional<EmployerResp> findById(Long id) {
        return employerRepo.findById(id).map(facade::toEmployerResp);
    }

    public EmployerResp save(Long cId, EmployerReq eReq) {
        Employer e = customerRepo.findById(cId).map(c -> {
            Employer employer = facade.toEmployer(eReq);
            c.addEmployer(employer);
            return employerRepo.save(employer);
        }).orElseThrow(() -> new RuntimeException("Employer wasn't saved"));

        return facade.toEmployerResp(employerRepo.save(e));
    }

    public boolean  deleteEmployerFromCustomer(Long cId, Long eId) {
        try {
            Customer customer = customerRepo.findById(cId).orElseThrow(() -> new RuntimeException("Customer not found"));
            customer.removeEmployer(eId);
            customerRepo.save(customer);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean deleteById(Long id) {
        try {
            employerRepo.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
