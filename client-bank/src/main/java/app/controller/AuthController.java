package app.controller;

import app.configuration.security.JwtTokenService;
import app.dto.auth.LoginRq;
import app.dto.auth.LoginRs;
import app.dto.customer.CustomerReq;
import app.dto.customer.CustomerResp;
import app.repo.CustomerRepo;
import app.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
@EnableWebMvc
public class AuthController {
    private final CustomerService service;
    private final CustomerRepo repo;
    private final PasswordEncoder enc;
    private final JwtTokenService tokenService;

//    @PostMapping("/registration")
//    public ResponseEntity<?> handleCreate(@RequestBody CustomerReq rq) {
//        service.save(rq);
//        return ResponseEntity.ok().build();
//    }

    @PostMapping("/login")
    public ResponseEntity<LoginRs> handleLogin(@RequestBody LoginRq rq) {
        return repo.findCustomerByEmail(rq.getEmail())
                .filter(u -> enc.matches(rq.getPassword(), u.getPassword()))
                .map(u -> tokenService.generateToken(Math.toIntExact(u.getId())))
                .map(LoginRs::Ok)
                .map(ResponseEntity::ok)
                .orElse(
                        ResponseEntity
                                .status(HttpStatus.FORBIDDEN)
                                .body(LoginRs.Error("wrong user/password combination"))
                );
    }
}
