package app.configuration.security;

import app.models.Customer;
import app.repo.CustomerRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@Log4j2
@Configuration
@RequiredArgsConstructor
public class UserDetailsServiceJPA implements UserDetailsService {
    private final CustomerRepo repo;
    private UserDetails mapper(Customer c) {
        return User
                .withUsername(c.getEmail())
                .password(c.getPassword())
                .roles()
                .build();
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        log.info("-------- loading user: {}", email);
        return repo.findCustomerByEmail(email)
                .map(this::mapper)
                .orElseThrow(() -> new UsernameNotFoundException(
                        String.format("user with email %s was not found", email)
                ));
    }
}
