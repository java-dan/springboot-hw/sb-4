package app.configuration.security;

import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
@PropertySource("classpath:jwt.properties")
public class JwtTokenService {
    @Value("${jwt.secret}")
    private String secret;

    private static Long day = 60 * 60 * 24 * 1000L;
    private static Long week = day * 7;

    public String generateToken(Integer userId, boolean rememberMe) {
        Date now = new Date();
        Date expiry = new Date(now.getTime() + (rememberMe ? week : day));
        return Jwts.builder()
                .setSubject(userId.toString()) // ANY string / JSON / whatever
                .setIssuedAt(now)
                .setExpiration(expiry)
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    public String generateToken(Integer userId) {
        return generateToken(userId, false);
    }

    private Optional<Jws<Claims>> parseTokenToClaims(String token) {
        try {
            return Optional.of(Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token));
        } catch (JwtException x) {
            System.out.println("Something went wrong with token");
            return Optional.empty();
        }
    }

    public Optional<Long> parseToken(String token) {
        return parseTokenToClaims(token)
                .map(Jwt::getBody)
                .map(Claims::getSubject)
                .map(Long::parseLong);
    }
}
